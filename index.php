<?php require_once 'header.php'; ?>

<p class="introduction">Camapania „Pirații nu folosesc calculatoare!” este lansată cu scopul de a oferi o critică campaniei „Stop pirateria și contrafaceara” lansată de către Agenția de Stat pentru Proprietatea Intelectuală prin interemediul sit-ului www.stoppirateria.md.</p>

<?php require_once 'sidebar.php'; ?>

<div id="main-menu">
    <a class="item-1" href="#pcpi"><p>Ce este „pirateria”, contrafacerea, „proprietatea intelectuală”</p></a></li>
    <a class="item-2" href="#programe-libere"><p>Programe libere</p></a></li>
    <a class="item-3" href="#cultura-libera"><p>Cultura liberă</p></a></li>
    <a class="item-4" href="#materiale"><p>Materiale</p></a></li>
    <a class="item-5" href="#promoveaza"><p>Promovează</p></a></li>
    <a class="item-6" href="#despre"><p>Despre</p></a></li>
</div> <!-- #main-menu -->
<div id="popup">
    <!-- <img src="close.png" alt="[x]" class="close-button" /> -->
    <div class="close-button"><p>X</p></div>
    <div id="popup-content">
    </div>
</div>
<?php require_once 'footer.php'; ?>