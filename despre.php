<?php require_once 'header.php'; ?>

<h2>Despre</h2>

<p>Campania „Pirații nu folosesc calculatoare!” a fost lansată de către comunitatea pentru programe și arte libere <a href="http://ceata.org">Ceata</a> (grupul din Republia Moldova). Noi doriim să oferim o critică capaniei „stop pirateria și contrafaferea!” lansată de către Agenția de Stat pentru Proprietatea Intelectuală și să infromăm soietatea cu privire libertățile în folosirea programelor și lucrărilor de creație, clarificarea unor termeni foarte des utilizați de AGEPI, dar care sunt absolut greșiți.</p>

<h3>Ce este Ceata</h3>

<p><a href="http://ceata.org">Ceata</a> este o comunicate românească care promovează artele și tehnologiile libere. Creațiile artistice și tehnologice licențiate liber pot fi folosite, studiate, modificate și distribuite de oricine în orice scop, fără a necesita permisiuni din partea autorilor. Ceata folosește licențe puternice pentru creațiile membrilor săi, astfel încât creațiile odată eliberate să rămână libere de restricții și autorii lor să se bucure de recunoașterea contribuțiilor aduse.

<p><a href="http://md.ceata.org/">Ceata din Moldova</a> este grupul din Republica Moldova al membrilor comunității Ceata. Scopul acesteia este să organizeze evenimente locale în concordanță cu principiile comunității.</p>

<h3>Cum ne puteți ajuta?</h3>

<p>Dacă credeți în libertatea programelior și culturii vă inivităm să aderați la comunitatea noastră. Dacă sunteți prea ocupat, ne puteți oferi sprijinul a face o donație către comunitate. Toate contribuțiile dvs. vor merge la sustinerea și lansarea proiectelor pentru promovarea și dezvoltatea programelor și careațiilor libere.</p>


<?php require_once 'footer.php'; ?>

