<div class="clear"></div>
</div> <!-- content -->
</div> <!-- #container -->

<div id="footer">
   <a href="http://creativecommons.org/licenses/by-sa/3.0/ro/" rel="license" target="_blank" >
       <img alt="" src="http://i.creativecommons.org/l/by-sa/3.0/88x31.png" />
   </a><br />
    <!--[if lte IE 8]><span style="filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=2); display: inline-block;"><![endif]-->
    <span style="font-size: 1.5em; font-weight: bold; -webkit-transform: rotate(180deg); -moz-transform: rotate(180deg); -o-transform: rotate(180deg); -khtml-transform: rotate(180deg); -ms-transform: rotate(180deg); transform: rotate(180deg); display: inline-block;">
            &copy;
    </span> <? echo date('Y'); ?>
    <!--[if lte IE 8]></span><![endif]-->
     Ceata. Toate materialele pot fi copiate, modificate, distribuite și folosite în scopuri comerciale conform condițiilor specificate de licența
    <a href="http://creativecommons.org/licenses/by-sa/3.0/ro/">CC-BY-SA 3.0</a>.
</div>

<script type="text/javascript">

    $('a[class^=item-]').click(function(){
        href = $(this).attr('href').replace('#','');
        $('#popup-content').load(href+'.html');
        //$('#popup').show(250);
        $('#popup').fadeIn(500);
    });

    $(document).keydown(function(e) {
        if (e.keyCode == 27) {
            //$('#popup').hide(150);
            $('#popup').fadeOut(250);
        }
    });

    $('.close-button').click(function(){
        $('#popup').fadeOut(250);
    });

    if(window.location.hash) {
        href = window.location.hash.replace('#','');
        $('#popup-content').load(href+'.html');
        $('#popup').fadeIn(500);
    } else {
      // Fragment doesn't exist
    }
</script>

</body>
</html>

